import React from 'react';
import '../Landing/landing.css';
import { Link } from 'react-router-dom';
import Button from '../button/btn';

function Landing(){
    return(
        <div id="mainDiv">
            <h1 className="mainHedding">Let 's Chat</h1>
            <p>Simple Chat application where you can Do much more than normal.</p>
            <div id="mainLanding">
                <Link to="/login">{<Button btnName={"Log In"} type={"login"} />}</Link>
                <Link to="/register">{<Button btnName={"Registger"} type={"register"} />}</Link>
            </div>
        </div>
    )
}

export default Landing;