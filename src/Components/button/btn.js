import React from 'react';
import '../button/btn.css';

const Btn = (props) => {
    let name = (props) => {
        // if(props.type === "login")
        // {
        //     return "btn login";
        // }
        switch(props.type){
            case "login":
                return "btn login"
            case "register":
                return "btn register"
            default:
                return "btn"
        }
    }
    return(
        <button className={name(props)} >{props.btnName}</button>
    );
}

export default Btn;